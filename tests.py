#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/19
    Time: 15:02
"""
import os
import unittest

from app import app, db
from app.models import User
from config import BASE_DIR


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config["SQLALCHEMY_DATBASE_URI"] = 'sqlite:///' + os.path.join(BASE_DIR, "test.db")
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_avatar(self):
        u = User(nickname="john", email='john@email.com')
        avatar = u.avatar(128)
        expected = 'http://www.gravatar.com/avatar/d4c74594d841139328695756648b6bd6'
        assert avatar[0:len(expected)] == expected

    def test_make_unique_nickname(self):
        u = User(nickname="john", email="john@email.com")
        db.session.add(u)
        db.session.commit()
        nickname = User.make_unique_nickname('john')
        assert nickname != 'john'

        u = User(nickname=nickname, email='susan@email.com')
        db.session.add(u)
        db.session.commit()
        nickname2 = User.make_unique_nickname('john')
        assert nickname2 != 'john'
        assert nickname2 != nickname


if __name__ == "__main__":
    unittest.main()
